public class Main {

  public static void main(String[] args) {

    System.out.println("Hello world!");

    String name = "Bóra";

    System.out.println(name);

    for (int i = 0; i < 4; i++) {
      System.out.println(i);
    }

    if(name == "Orsi") {
      System.out.println("Igaz");
    } else {
      System.out.println("Hamis");
    }

    //method calls here
    sayHello();
    System.out.println(add(2,3));

    //create object from class:
    Dog myDog = new Dog();
    myDog.bark();

  }

  //method
  static void sayHello() {
    System.out.println("hello");
  }

  //method with return
  static int add(int a, int b) {
    return a +b;
  }
}